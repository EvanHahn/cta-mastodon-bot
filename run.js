const path = require('path')
const fs = require('fs')
const {URL} = require('url')
const https = require('https')
const Mastodon = require('megalodon').default

const BASE_URL = 'https://botsin.space'

const client = new Mastodon(process.env.MASTODON_ACCESS_TOKEN, BASE_URL + '/api/v1')

const alertIdsSeenPath = path.join(__dirname, 'alert_ids_seen.json')

async function fetchAlerts () {
  const url = new URL('https://www.transitchicago.com/api/1.0/alerts.aspx')
  url.searchParams.set('activeonly', 'TRUE')
  url.searchParams.set('outputType', 'JSON')

  return new Promise((resolve, reject) => {
    https.get(url, (res) => {
      if (res.statusCode !== 200) {
        res.resume() // to free up memory
        reject(new Error('Expected status code 200 but got ' + res.statusCode))
        return
      }

      res.setEncoding('utf8')

      let rawData = ''
      res.on('data', (chunk) => { rawData += chunk })
      res.on('end', () => {
        try {
          const parsed = JSON.parse(rawData)
          const errorMessage = parsed.CTAAlerts.ErrorMessage
          if (errorMessage) {
            reject(new Error(errorMessage))
            return
          }
          resolve(parsed.CTAAlerts.Alert)
        } catch (err) {
          reject(err)
        }
      })
    }).on('error', reject)
  })
}

function getAlertIdsSeen () {
  let fileData
  try {
    fileData = fs.readFileSync(alertIdsSeenPath)
  } catch (err) {
    if (err.code === 'ENOENT') {
      return new Set()
    } else {
      throw err
    }
  }
  return new Set(JSON.parse(fileData))
}

function saveAlertIdsSeen (alertIdsSet) {
  const alertIdsArray = [...alertIdsSet]
  const data = JSON.stringify(alertIdsArray)
  fs.writeFileSync(alertIdsSeenPath, data)
}

async function postToot (status) {
  console.log('Posting', status)
  await client.post('/statuses', { status })
  console.log('Posted')
}

function isNonblankString (str) {
  return (typeof str === 'string') && Boolean(str.trim().length)
}

function formatAlert (alert) {
  const result = []

  if (isNonblankString(alert.Headline)) {
    result.push(alert.Headline.trim())
    if (isNonblankString(alert.ShortDescription)) {
      result.push(':')
    }
    result.push(' ')
  }

  if (isNonblankString(alert.ShortDescription)) {
    result.push(alert.ShortDescription.trim(), ' ')
  }

  if (isNonblankString(alert.AlertURL && alert.AlertURL['#cdata-section'])) {
    result.push(alert.AlertURL['#cdata-section'])
  }

  return result
    .join('')
    .trim()
    .replace(/\r?\n/g, ' ')
    .replace(/\s+/g, ' ')
}

async function main () {
  if (!process.env.MASTODON_ACCESS_TOKEN) {
    throw new Error('MASTODON_ACCESS_TOKEN env variable missing')
  }

  const alerts = await fetchAlerts()

  const alertIdsSeen = getAlertIdsSeen()

  const alertsToToot = alerts.filter(alert => !alertIdsSeen.has(alert.AlertId))

  alertsToToot.forEach(alert => alertIdsSeen.add(alert.AlertId))
  saveAlertIdsSeen(alertIdsSeen)

  const toots = alertsToToot.map(formatAlert)

  await Promise.all(toots.map(toot => postToot(toot)))
}

main()
